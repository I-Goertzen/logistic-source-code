<?php
declare(strict_types=1);

namespace UdgLogistic\Data;

use Shopware\Components\Cart\BasketHelperInterface;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Attribute\Article as AttributeArticle;
use Shopware\Models\Country\Country;
use Shopware\Models\Country\State;
use Shopware\Models\Customer\Address;
use Shopware\Models\Customer\Customer;
use Shopware\Models\Customer\Group as CustomerGroup;
use Shopware\Models\Dispatch\Dispatch;
use Shopware\Models\Order\Billing;
use Shopware\Models\Order\Detail;
use Shopware\Models\Order\DetailStatus;
use Shopware\Models\Order\Order as OrderEntity;
use Shopware\Models\Order\Shipping;
use Shopware\Models\Order\Status;
use Shopware\Models\Payment\Payment;
use Shopware\Models\Shop\Shop;
use Shopware\Models\Partner\Partner;
use Shopware\Models\Voucher\Voucher;
use Shopware\Models\Voucher\Code as VoucherCode;
use UdgLogistic\Exception\CsvImportException;
use UdgLogistic\Exception\DataGenerationException;
use UdgLogistic\Service\OrderItemTypeIdentifier;
use UdgMerchants\Service\StockSplitting;

/**
 * Data mapping object for orm-entity to csv.
 */
class Order implements CsvInterface
{

    /**
     * @var OrderEntity
     */
    private $entity;

    /**
     * @var Address
     */
    private $address;

    /**
     * @var \DateTime
     */
    private $dateExported;

    /**
     * @var \Shopware_Components_Config
     */
    private $configService;

    /**
     * @var ModelManager
     */
    private $modelsService;

    /**
     * @var Partner
     */
    private $partner;

    /**
     * @var $orderItemType
     */
    private $orderItemType;

    /**
     * Creates the Object from an orm entity.
     *
     * @param OrderEntity $entity
     * @param \DateTime   $dateExported
     *
     * @return Order
     */
    public static function createFromEntity(
        OrderEntity $entity,
        \DateTime $dateExported = null
    ): self {

        $order = new self();

        $order->entity = $entity;
        $order->dateExported = $dateExported;
        return $order;
    }

    /**
     * Get the current entity.
     *
     * @return OrderEntity
     */
    public function getEntity(): OrderEntity
    {
        return $this->entity;
    }

    /**
     * Inject Config.
     *
     * @param \Shopware_Components_Config $config
     */
    public function setConfigService(\Shopware_Components_Config $config): void
    {
        $this->configService = $config;
    }

    /**
     * Inject model manager.
     *
     * @param ModelManager $models
     */
    public function setModelsService(ModelManager $models): void
    {
        $this->modelsService = $models;
    }

    /**
     * Get array with column ordering
     *
     * @return array
     */
    public function getColumnOrder(): array
    {
        return [
            'Status',
            'Shopware Bestellnummer',
            'GLN',
            'Portal-Nummer',
            'ERP-Nummer',
            'Referenz',
            'Betrag',
            'Titel',
            'Adresszusatz1',
            'Adresszusatz2',
            'Shopwarebenutzer ID',
            'Firma',
            'Abteilung',
            'Name',
            'Straße, Hausnr',
            'PLZ',
            'Stadt',
            'Telefon',
            'Land',
            'Bundesland',
            'E-Mail',
            'Alternative Benutzer ID',
            'Wunschlieferdatum',
            'Lieferscheinnummer',
            'voraussichtliches Lieferdatum',
            'Shopware Bestell Zeit',
            'Exportzeitpunkt',
            'EAN',
            'Artikelname',
            'MTS Artikelnummer', //mts artikelnumber currently always empty
            'Positionsnummer',
            'Käufer Artikelnummer',
            'Eingabe-Nr.',
            'Packstückkennzeichnung',
            'Aufklebertyp',
            'Sendungsident',
            'Location',
            'Retourenschlüssel',
            'Steuerrate in %',
            'Preis',
            'Menge',
            'Besonderheiten',
            'Endkundennummer',
            'Versandkosten (netto)',
        ];
    }

    /**
     * Get all csv rows for this order (multiple for details/variants)
     *
     * @return array
     * @throws DataGenerationException
     */
    public function toArrayInColumnOrder(string $orderType = ''): array
    {
        $data = [];

        $baseValues = $this->getBaseValues();

        /** @var Detail $article */
        foreach ($this->entity->getDetails() as $article) {
            $outValue = [];
            $this->orderItemType = new OrderItemTypeIdentifier();
            $this->orderItemType = $this->orderItemType->checkOrderItem($article);

            // only export valid products
            if($orderType === 'order-logistics-csv' && $this->orderItemType !== "valid_product") {
                continue;
            }
            // only export valid products and value vouchers
            elseif ($orderType === 'order-sap-csv' && $this->orderItemType === "invalid") {
                continue;
            }

            $articleValues = $this->getArticleDetailValues($article);

            foreach ($this->getColumnOrder() as $columnName) {
                if (array_key_exists($columnName, $baseValues)) {
                    $outValue[] = $baseValues[$columnName];
                } elseif (array_key_exists($columnName, $articleValues)) {
                    $outValue[] = $articleValues[$columnName];
                } else {
                    throw new DataGenerationException(sprintf('Missing columnvalue for column: %s', $columnName));
                }
            }

            $data[] = $outValue;
        }

        /* SORT DATA BY SHIPPING-CODE (ArrayKey 34 = ShippingCode) */
        usort($data, function ($x, $y, $key = 34) {
            return $x[$key] < $y[$key] ? -1 : $x[$key] != $y[$key];
        });

        return $data;
    }

    /**
     * Get base information.
     *
     * @return array
     */
    private function getBaseValues(): array
    {
        /** @var \Shopware\Models\Attribute\Order $attribute */
        $attributes = $this->entity->getAttribute();

        /** @var Shipping $shipping */
        $shipping = $this->entity->getShipping();

        /** @var Billing $billing */
        $billing = $this->entity->getBilling();

        $phone = (string)$shipping->getPhone();
        if ($phone !== '' && $billing instanceof Billing) {
            $phone = $billing->getPhone();
        }

        $gln = $attributes->getUdgDsMerchant();
        if (empty($gln) &&
            $this->entity->getTransactionId() !== '' &&
            !in_array(
                $this->entity->getDeviceType(),
                ['desktop', 'tablet', 'tabletLandscape', 'mobile', 'mobileLandscape']
            )
        ) {
            $gln = $this->entity->getDeviceType();
        }
        if (empty($gln)) {
            $gln = StockSplitting::WEBSHOP_GLN;
        }

        /** @var State $shippingState */
        $shippingState = $shipping->getState();

        return [
            'Status'                        => $this->entity->getOrderStatus()->getId(),
            'Shopware Bestellnummer'        => 'sw' . $this->entity->getNumber(),
            'GLN'                           => $gln,
            'Portal-Nummer'                 => $attributes->getUdgDsPortalnumber(),
            'ERP-Nummer'                    => $attributes->getUdgDsErpnumber(),
            'Referenz'                      => $this->entity->getTransactionId(),
            'Betrag'                        => $this->entity->getInvoiceAmount(),
            'Titel'                         => $shipping->getTitle(),
            'Adresszusatz1'                 => $shipping->getAdditionalAddressLine1(),
            'Adresszusatz2'                 => $shipping->getAdditionalAddressLine2(),
            'Shopwarebenutzer ID'           => $shipping->getCustomer()->getId(),
            'Alternative Benutzer ID'       => $attributes->getUdgDsAdditionalcustomernumber(),
            'Firma'                         => $shipping->getCompany(),
            'Abteilung'                     => $shipping->getDepartment(),
            'Name'                          => trim($shipping->getFirstName() . ' ' . $shipping->getLastName()),
            'Straße, Hausnr'                => $shipping->getStreet(),
            'PLZ'                           => $shipping->getZipCode(),
            'Stadt'                         => $shipping->getCity(),
            'Telefon'                       => $phone,
            'Land'                          => $shipping->getCountry()->getIso(),
            'Bundesland'                    => ($shippingState !== null) ? $shippingState->getName() : '',
            'E-Mail'                        => $this->entity->getCustomer()->getEmail(),
            'Wunschlieferdatum'             => $attributes->getUdgDsWishdeliverydate(),
            'Lieferscheinnummer'            => $attributes->getUdgDsReceiptnumber(),
            'voraussichtliches Lieferdatum' => $attributes->getUdgDsDeliverydate(),
            'Shopware Bestell Zeit'         => $this->entity->getOrderTime()
                ? $this->entity->getOrderTime()->format('Ymd_His') : '',
            'Exportzeitpunkt'               => $this->dateExported->format('Ymd_His_v'), // export time
            'Endkundennummer'               => $attributes->getUdgDsCustomernumberforendcustomer(),
            'Versandkosten (netto)'         => $this->entity->getInvoiceShippingNet(),
        ];
    }

    /**
     * Get article detail information.
     *
     * @param Detail $orderDetail
     *
     * @return array
     */
    private function getArticleDetailValues(Detail $orderDetail): array
    {
        if($this->orderItemType === "valid_product") {
            return [
                'EAN'                    => $orderDetail->getEan(),
                'Artikelname'            => $orderDetail->getArticleName(),
                'MTS Artikelnummer'      => $this->getUdgDsMtsArticlenumber($orderDetail),
                'Positionsnummer'        => $orderDetail->getAttribute()->getUdgDsPositionumber(),
                'Käufer Artikelnummer'   => $orderDetail->getAttribute()->getUdgDsBuyerarticlenumber(),
                'Eingabe-Nr.'            => $orderDetail->getAttribute()->getUdgDsDocumentnumber(),
                'Packstückkennzeichnung' => $orderDetail->getAttribute()->getUdgDsPackageidentification(),
                'Aufklebertyp'           => $orderDetail->getArticleDetail()->getAttribute()->getUdgProductShippingCode(),
                'Sendungsident'          => $orderDetail->getAttribute()->getUdgDsBroadcastident(),
                'Location'               => $orderDetail->getAttribute()->getUdgDsLocation(),
                'Retourenschlüssel'      => $orderDetail->getAttribute()->getUdgDsReturnkey(),
                'Steuerrate in %'        => $orderDetail->getTaxRate(),
                'Preis'                  => $this->getNetPriceWithVoucher($orderDetail),
                'Menge'                  => $orderDetail->getQuantity(),
                'Besonderheiten'         => $orderDetail->getConfig(),
                'Versandkosten (netto)'  => $this->entity->getInvoiceShippingNet(),
            ];
        }
        else if($this->orderItemType === "valid_voucher"){
            return [
                'EAN'                    => '',
                'Artikelname'            => $orderDetail->getArticleName(),
                'MTS Artikelnummer'      => '',
                'Positionsnummer'        => $orderDetail->getAttribute()->getUdgDsPositionumber(),
                'Käufer Artikelnummer'   => '',
                'Eingabe-Nr.'            => '',
                'Packstückkennzeichnung' => '',
                'Aufklebertyp'           => '',
                'Sendungsident'          => '',
                'Location'               => '',
                'Retourenschlüssel'      => '',
                'Steuerrate in %'        => $orderDetail->getTaxRate(),
                'Preis'                  => number_format($orderDetail->getPrice(), 2, '.', ''),
                'Menge'                  => $orderDetail->getQuantity(),
                'Besonderheiten'         => $orderDetail->getConfig(),
                'Versandkosten (netto)'  => $this->entity->getInvoiceShippingNet(),
            ];
        }
        else
        {
            return [
                'EAN'                    => '',
                'Artikelname'            => '',
                'MTS Artikelnummer'      => '',
                'Positionsnummer'        => '',
                'Käufer Artikelnummer'   => '',
                'Eingabe-Nr.'            => '',
                'Packstückkennzeichnung' => '',
                'Aufklebertyp'           => '',
                'Sendungsident'          => '',
                'Location'               => '',
                'Retourenschlüssel'      => '',
                'Steuerrate in %'        => '',
                'Preis'                  => '',
                'Menge'                  => '',
                'Besonderheiten'         => '',
                'Versandkosten (netto)'  => '',
            ];
        }
    }

    /**
     * Get the net price incl. voucher.
     *
     * @param Detail $orderDetail
     *
     * @return float
     */
    private function getNetPriceWithVoucher(Detail $orderDetail): float
    {
        $price = $orderDetail->getPrice() / (1 + ($orderDetail->getTaxRate() / 100));

        $orderSupplierId = $orderDetail->getArticleDetail()->getArticle()->getSupplier()->getId();
        $orderArticleNumber = $orderDetail->getArticleDetail()->getNumber();

        foreach ($this->entity->getDetails() as $orderD) {
            /** @var Detail $orderD */
            if ($orderD->getMode() > 0) {
                if ($orderD->getMode() === BasketHelperInterface::DISCOUNT_PERCENT) {
                    $repository = $this->modelsService->getRepository(VoucherCode::class);
                    $voucherCode = $repository->find($orderD->getArticleId());

                    if (($voucherCode instanceof VoucherCode) === false) {
                        continue;
                    }
                    $voucher = $voucherCode->getVoucher();
                    if (($voucher instanceof Voucher) === false) {
                        continue;
                    }

                    $calcDiscount = false;

                    if (empty($voucher->getStrict())) {
                        $calcDiscount = true;
                    } else {
                        $allowedSupplierId = $voucher->getBindToSupplier();
                        $allowedProducts = array_filter(explode(';', $voucher->getRestrictArticles()));
                        if (!empty($allowedProducts) && !empty($allowedSupplierId)) {
                            if (in_array($orderArticleNumber, $allowedProducts)
                                && $orderSupplierId == $allowedSupplierId
                            ) {
                                $calcDiscount = true;
                            }
                        } elseif (!empty($allowedProducts)) {
                            if (in_array($orderArticleNumber, $allowedProducts)) {
                                $calcDiscount = true;
                            }
                        } elseif (!empty($allowedSupplierId)) {
                            if ($orderSupplierId == $allowedSupplierId) {
                                $calcDiscount = true;
                            }
                        }
                    }

                    if ($calcDiscount) {
                        $price = $price * ((100 - $voucher->getValue()) / 100);
                        break;
                    }
                }
            }
        }

        $price = round($price, 2);

        return $price;
    }

    /**
     * @param Detail $articleDetail
     *
     * @return string
     */
    private function getUdgDsMtsArticlenumber(Detail $articleDetail): string
    {
        //@TODO - To clarify with MTS: What should happen with Shopware internal articles during export.
        if ($this->isShopwareInternalArticle($articleDetail)) {
            return '';
        }

        $repository = $this->modelsService->getRepository(AttributeArticle::class);
        $attribute = $repository->findOneBy([
            'articleId'       => $articleDetail->getArticleId(),
            'articleDetailId' => $articleDetail->getArticleDetail()->getId(),
        ]);

        if ($attribute === null || !$attribute instanceof AttributeArticle) {
            return '';
        }

        return (string)$attribute->getUdgDsMtsarticlenumber();
    }

    /**
     * Check if article is used for Shopware representation of discount codes, payment and general basket discounts
     *
     * @param Detail $article
     *
     * @return bool
     */
    private function isShopwareInternalArticle(Detail $article): bool
    {
        /**
         * Shopware internal Article numbers / IDs
         * used to represent discount codes, payment discounts/charges and general basket discounts
         */
        $shopwareInternalArticleNumbers = [
            $this->configService->get('sPAYMENTSURCHARGEABSOLUTENUMBER', 'PAYMENTSURCHARGEABSOLUTENUMBER'),
            $this->configService->get('sDISCOUNTNUMBER', 'DISCOUNT'),
            $this->configService->get('sSHIPPINGDISCOUNTNUMBER', 'SHIPPINGDISCOUNT'),
            $this->configService->get('sPAYMENTSURCHARGENUMBER', 'PAYMENTSURCHARGE'),
        ];

        $shopwareInternalArticleIds = [
            0,
            1,
        ];

        return in_array($article->getArticleNumber(), $shopwareInternalArticleNumbers, true) ||
            in_array($article->getArticleId(), $shopwareInternalArticleIds, true);
    }

    /**
     * Update order with data from csv.
     *
     * @param array $data
     *
     * @throws CsvImportException
     */
    public function updateEntityWithDataArray(array $data): void
    {
        $columnsNotToImport = ['Aufklebertyp', 'Packstückkennzeichnung', 'Sendungsident', 'Versandkosten (netto)'];

        $columnDiff = array_diff($this->getColumnOrder(), array_keys($data), $columnsNotToImport);

        if (count($columnDiff) > 0) {
            throw new CsvImportException(sprintf('Missing column "%s" in input data.', array_pop($columnDiff)));
        }


        if (!$this->entity->getCustomer() instanceof Customer) {
            $this->initCustomer($data);
            $this->initAddress($data);
            $this->initPartner('Drop Shipment');
            $this->initOrder($data);
        }

        $this->setArticleDetailValues($data);
        $this->entity->updateChangedTimestamp();
    }

    /**
     * @return Address
     */
    public function getAddress(): Address
    {

        return $this->address;
    }

    /**
     * Init an customer new entity.
     *
     * @param array $data
     *
     * @throws CsvImportException
     */
    private function initCustomer(array $data): void
    {
        $customer = new Customer();

        $customer->setAccountMode(1);

        $customer->setLastname(trim($data['Name']));
        $customer->setFirstname(' ');

        if ($data['E-Mail'] !== '') {
            $customer->setEmail($data['E-Mail']);
        } else {
            // TODO use setting
            $customer->setEmail('no-email@mts.de');
        }

        $customer->setSalutation('mr');

        $shopRepository = $this->modelsService->getRepository(Shop::class);
        $shopEntity = $shopRepository->getDefault();
        $customer->setShop($shopEntity);

        $customer->setGroup($this->getCustomerGroup());

        $this->entity->setCustomer(
            $customer
        );
    }

    /**
     * Init address from data.
     *
     * @param array $data
     *
     * @throws CsvImportException
     */
    private function initAddress(array $data): void
    {

        $this->address = new Address();
        $this->address->setTitle($data['Titel']);
        $this->address->setAdditionalAddressLine1($data['Adresszusatz1']);
        $this->address->setAdditionalAddressLine2($data['Adresszusatz2']);
        $this->address->setCompany($data['Firma']);
        $this->address->setDepartment($data['Abteilung']);

        $this->address->setSalutation($this->entity->getCustomer()->getSalutation());
        $this->address->setFirstname($this->entity->getCustomer()->getFirstname());
        $this->address->setLastname($this->entity->getCustomer()->getLastname());
        $this->address->setStreet($data['Straße, Hausnr']);
        $this->address->setZipcode($data['PLZ']);
        $this->address->setCity($data['Stadt']);
        $this->address->setPhone($data['Telefon']);

        $country = $this->modelsService->getRepository(Country::class)->findOneBy([
            'iso' => $data['Land'],
        ]);
        if (!$country instanceof Country) {
            throw new CsvImportException(sprintf('Can not find a country for `%s`', $data['Land']));
        }
        $this->address->setCountry($country);

        if ($data['Bundesland'] !== '') {
            $state = $this->modelsService->getRepository(State::class)->findOneBy([
                'shortCode' => $data['Bundesland'],
            ]);
            if (!$state instanceof State) {
                throw new CsvImportException(sprintf('Can not find a state for `%s`', $data['Bundesland']));
            }
            $this->address->setState($state);
        }
    }

    /**
     * init order
     *
     * @param array $data
     *
     * @throws \Exception
     */
    private function initOrder(array $data): void
    {
        // Setting default values, necessary because of not-nullable table colums
        $this->entity->setComment('');
        $this->entity->setCustomerComment('');
        $this->entity->setInternalComment('');
        $this->entity->setTemporaryId('');
        $this->entity->setTransactionId($data['Referenz']);
        $this->entity->setTrackingCode('');
        $this->entity->setReferer('');

        $this->entity->setCurrency('EUR');
        $this->entity->setCurrencyFactor(1);
        $this->entity->setLanguageIso('1');
        $this->entity->setTaxFree(0);
        $this->entity->setNet(0);
        $this->entity->setInvoiceAmount(0);
        $this->entity->setInvoiceAmountNet(0);
        $this->entity->setInvoiceShipping(0);
        $this->entity->setInvoiceShippingNet(0);
        $this->entity->setOrderStatus(
            $this->modelsService->getRepository(Status::class)->findOneBy([
                'id'    => Status::ORDER_STATE_OPEN,
                'group' => 'state',
            ])
        );
        $this->entity->setPaymentStatus(
            $this->modelsService->getRepository(Status::class)->findOneBy([
                'id'    => Status::PAYMENT_STATE_OPEN,
                'group' => 'payment',
            ])
        );

        $this->entity->setShop(
            $this->entity->getCustomer()->getShop()
        );
        $this->entity->setDispatch(
            $this->modelsService->getRepository(Dispatch::class)->findOneBy([
                // Standardversand
                'id'     => 9,
                'active' => 1,
            ])
        );
        $this->entity->setPayment(
            $this->modelsService->getRepository(Payment::class)->findOneBy([
                'name' => 'invoice',
            ])
        );
        $this->entity->setOrderTime(new \DateTime);

        $this->entity->setDeviceType('Drop Shipment');
        $this->entity->setPartner($this->partner);

        // add order attributes
        $orderAttribute = new \Shopware\Models\Attribute\Order();
        foreach ([
                     'GLN'                           => 'udgDsMerchant',
                     'Portal-Nummer'                 => 'udgDsPortalnumber',
                     'ERP-Nummer'                    => 'udgDsErpnumber',
                     'Wunschlieferdatum'             => 'udgDsWishdeliverydate',
                     'Lieferscheinnummer'            => 'udgDsReceiptnumber',
                     'voraussichtliches Lieferdatum' => 'udgDsDeliverydate',
                     'Alternative Benutzer ID'       => 'udgDsAdditionalcustomernumber',
                     'Endkundennummer'               => 'udgDsCustomernumberforendcustomer',
                 ] as $arrayKey => $propertyName) {
            $propertySetter = 'set' . ucfirst($propertyName);
            $orderAttribute->$propertySetter($data[$arrayKey]);
        }
        $this->entity->setAttribute($orderAttribute);
    }

    /**
     * Set the order article details.
     *
     * @param array $data
     *
     * @throws CsvImportException
     */
    private function setArticleDetailValues(array $data): void
    {

        $orderArticleDetail = new Detail();
        $orderArticleDetail->setOrder($this->entity);

        /** @var \Shopware\Models\Article\Detail $articleDetail */
        $articleDetail = $this->modelsService->getRepository(\Shopware\Models\Article\Detail::class)->findOneBy([
            'ean' => $data['EAN'],
        ]);

        if (!$articleDetail instanceof \Shopware\Models\Article\Detail) {
            throw new CsvImportException(
                sprintf('Missing article for EAN %s', $data['EAN'])
            );
        }

        $orderArticleDetail->setArticleNumber($articleDetail->getNumber());
        $orderArticleDetail->setEan($articleDetail->getEan());
        $orderArticleDetail->setArticleId($articleDetail->getArticleId());
        $orderArticleDetail->setArticleDetail($articleDetail);
        $orderArticleDetail->setArticleName($articleDetail->getArticle()->getName());
        $orderArticleDetail->setTax($articleDetail->getArticle()->getTax());
        $orderArticleDetail->setTaxRate($articleDetail->getArticle()->getTax()->getTax());
        // TODO ?
        $orderArticleDetail->setPrice($data['Preis']);
        $orderArticleDetail->setQuantity((int)$data['Menge']);
        $orderArticleDetail->setConfig($data['Besonderheiten']);

        $orderArticleDetail->setStatus(
            $this->modelsService->getRepository(DetailStatus::class)->findOneBy(['id' => 0])
        );

        $attribute = new \Shopware\Models\Attribute\OrderDetail();

        foreach ([
                     'Positionsnummer'      => 'udgDsPositionumber',
                     'Käufer Artikelnummer' => 'udgDsBuyerarticlenumber',
                     'Eingabe-Nr.'          => 'udgDsDocumentnumber',
                     'Location'             => 'udgDsLocation',
                     'Retourenschlüssel'    => 'udgDsReturnkey',
                 ] as $columnKey => $attributeKey) {
            $setter = 'set' . ucfirst($attributeKey);
            $attribute->$setter($data[$columnKey]);
        }

        $orderArticleDetail->setAttribute($attribute);

        $this->entity->getDetails()->add($orderArticleDetail);
    }


    /**
     * @param string $idCode
     *
     * @throws \Exception
     */
    private function initPartner(string $idCode): void
    {
        // try to find partner
        $partner = $this->modelsService->getRepository(Partner::class)->findOneBy([
            'idCode' => $idCode,
        ]);

        if ($partner !== null) {
            $this->partner = $partner;
        } else {
            // make new partner if necessary
            $this->partner = new Partner();
            $this->partner->setIdCode($idCode);
            $this->partner->setDate(new \DateTime('Now'));
            // this is done for nicer display in Shopware BE
            $this->partner->setCompany($idCode);

            // set empty values
            $this->partner->setContact('');
            $this->partner->setStreet('');
            $this->partner->setCity('');
            $this->partner->setZipCode('');
            $this->partner->setCountryName('');
            $this->partner->setPhone('');
            $this->partner->setFax('');
            $this->partner->setEmail('');
            $this->partner->setWeb('');
            $this->partner->setProfile('');
            $this->partner->setActive(1);

            $this->modelsService->persist($this->partner);
        }
    }

    /**
     * Get the customer group.
     *
     * @return CustomerGroup
     * @throws CsvImportException
     */
    private function getCustomerGroup(): CustomerGroup
    {
        // try to find customer group
        $customerGroup = $this->modelsService->getRepository(CustomerGroup::class)->findOneBy([
            'key'  => 'DS',
            'name' => 'DS-Kunde',
        ]);

        if ($customerGroup instanceof CustomerGroup) {
            return $customerGroup;
        }

        throw new CsvImportException('Can not find customer group "DS"');
    }
}
