<?php
declare(strict_types=1);

namespace UdgLogistic\Data;

use Shopware\Components\Model\ModelManager;
use Shopware\Models\Attribute\Order as AttributeOrder;
use Shopware\Models\Dispatch\Dispatch;
use Shopware\Models\Order\Detail;
use Shopware\Models\Order\Order;
use UdgLogistic\Exception\CsvImportException;
use UdgLogistic\Exception\DataGenerationException;
use UdgLogistic\Service\OrderItemTypeIdentifier;
use UdgMerchants\Service\StockSplitting;


/**
 * Model for OrderStatus
 */
class OrderStatus implements CsvInterface
{
    /** @var Order */
    private $entity;
    /** @var ModelManager */
    private $modelsService;
    /** @var $orderItemType */
    private $orderItemType;

    public const DISPATCH_DS_COMMENT = 'Shipping used by drop shippment process - do not edit/delete!';
    public const DISPATCH_DS_STATUS = false; // active = false
    public const DISPATCH_DS_TYPE = 1;


    /**
     * Inject model manager.
     *
     * @param ModelManager $models
     */
    public function setModelsService(ModelManager $models): void
    {
        $this->modelsService = $models;
    }

    /**
     * Creates the Object from an orm entity.
     *
     * @param Order  $entity
     *
     * @return OrderStatus
     */
    public static function createFromEntity(
        Order $entity
    ): self {
        $order = new self();
        $order->entity = $entity;
        return $order;
    }

    /**
     * Get the order of columns/properties.
     * @return array
     */
    public function getColumnOrder(): array
    {
        return [
            'Shopware-Ordernummer',
            'GLN',
            'ERP-Nummer',
            'Portalnummer',
            'Shopware-Status',
            'Logistikunternehmen',
            'Sendungsverfolgungsnummer',
            'Notiz',
            'EAN',
            'Position',
            'Name des Warenempfängers',
            'Straße des Warenempfängers',
            'PLZ des Warenempfängers',
            'Stadt des Warenempfängers',
            'Land des Warenempfängers',
            'Adresszusatz1 des Warenempfängers',
            'Adresszusatz2 des Warenempfängers',
            'MTS Artikelnummer',
            'Käufer Artikelnummer',
            'Artikelbezeichnung',
            'Artikelmenge',
            'Mengen-Einheit',
            'Eingabe-Nr.',
            'Endkundennummer',
        ];
    }

    /**
     * Convert object to an array of the csv-row.
     * @return array
     * @throws DataGenerationException
     */
    public function toArrayInColumnOrder(string $orderType=''): array
    {
        $data = [];
        $baseValues = $this->getBaseValues();

        foreach ($this->entity->getDetails() as $article) {
            $outValue = [];

            $this->orderItemType = new OrderItemTypeIdentifier();
            $this->orderItemType = $this->orderItemType->checkOrderItem($article);

            // ignore voucher or null-articles for logistic order exports
            if($this->orderItemType !== "valid_product") {
                continue;
            }

            $detailValues = $this->getDetailValues($article);

            foreach ($this->getColumnOrder() as $columnName) {
                if (array_key_exists($columnName, $baseValues)) {
                    $outValue[] = $baseValues[$columnName];
                } elseif (array_key_exists($columnName, $detailValues)) {
                    $outValue[] = $detailValues[$columnName];
                } else {
                    throw new DataGenerationException(sprintf('Missing columnvalue for column: %s', $columnName));
                }

            }

            $data[] = $outValue;
        }

        return $data;
    }

    /**
     * Get basic order details like order number and status
     * @return array
     */
    private function getBaseValues(): array
    {
        /** @var \Shopware\Models\Attribute\Order $attribute */
        $attributes = $this->entity->getAttribute();

        $shipping = $this->entity->getShipping();

        $gln = $attributes->getUdgDsMerchant();
        if (empty($gln) &&
            $this->entity->getTransactionId() !== '' &&
            !in_array(
                $this->entity->getDeviceType(),
                ['desktop', 'tablet', 'tabletLandscape', 'mobile', 'mobileLandscape']
            )
        ) {
            $gln = $this->entity->getDeviceType();
        }
        if (empty($gln)) {
            $gln = StockSplitting::WEBSHOP_GLN;
        }

        $order = [
            'Order-Id'                          => $this->entity->getId(),
            'Shopware-Ordernummer'              => 'sw' . $this->entity->getNumber(),
            'GLN'                               => $gln,
            'ERP-Nummer'                        => $attributes->getUdgDsErpnumber(),
            'Portalnummer'                      => $attributes->getUdgDsPortalnumber(),
            'Shopware-Status'                   => $this->entity->getOrderStatus()->getId(),
            'Notiz'                             => str_replace(["\r\n","\r", "\n"], "<br />",$this->entity->getInternalComment()),
            'Name des Warenempfängers'          => trim($shipping->getFirstName() . ' ' . $shipping->getLastName()),
            'Straße des Warenempfängers'        => $shipping->getStreet(),
            'PLZ des Warenempfängers'           => $shipping->getZipCode(),
            'Stadt des Warenempfängers'         => $shipping->getCity(),
            'Land des Warenempfängers'          => $shipping->getCountry()->getIso(),
            'Adresszusatz1 des Warenempfängers' => $shipping->getAdditionalAddressLine1(),
            'Adresszusatz2 des Warenempfängers' => $shipping->getAdditionalAddressLine2(),
            'Endkundennummer'                   => $attributes->getUdgDsCustomernumberforendcustomer(),
        ];

        return $order;
    }
    
    /**
     * Get article detail information.
     *
     * @param Detail $detail
     *
     * @return array
     */
    private function getDetailValues(Detail $detail): array
    {
        if ($detail->getArticleDetail() instanceof \Shopware\Models\Article\Detail &&
            $detail->getArticleDetail()->getAttribute() instanceof \Shopware\Models\Attribute\Article) {
            $mtsarticlenumber = $detail->getArticleDetail()->getAttribute()->getUdgDsMtsarticlenumber();
            $shippingCode = $detail->getArticleDetail()->getAttribute()->getUdgProductShippingCode();
        } else {
            $mtsarticlenumber = '';
            $shippingCode = '';
        }

        if (isset($shippingCode) === false || empty($shippingCode)) {
            /** @var Dispatch $dispatch */
            $dispatch = $this->entity->getDispatch();

            $shippingCode = ($dispatch instanceof Dispatch) ? $dispatch->getName() : '';
        }

        return [
            'Logistikunternehmen'       => $shippingCode,
            'EAN'                       => $detail->getEan(),
            'Sendungsverfolgungsnummer' => $detail->getAttribute()->getUdgDsBroadcastident(),
            'Position'                  => $detail->getAttribute()->getUdgDsPositionumber(),
            'MTS Artikelnummer'         => $mtsarticlenumber,
            'Käufer Artikelnummer'      => $detail->getAttribute()->getUdgDsBuyerarticlenumber(),
            'Artikelbezeichnung'        => $detail->getArticleName(),
            'Artikelmenge'              => $detail->getQuantity(),
            'Mengen-Einheit'            => 'Stück',
            'Eingabe-Nr.'               => $detail->getAttribute()->getUdgDsDocumentnumber(),
        ];
    }

    /**
     * Update entity with Data
     *
     * @param array $data
     *
     * @return void
     * @throws CsvImportException
     */
    public function updateEntityWithDataArray(array $data): void
    {
        $columnsNotToImport = [
            'Name des Warenempfängers',
            'Straße des Warenempfängers',
            'PLZ des Warenempfängers',
            'Stadt des Warenempfängers',
            'Land des Warenempfängers',
            'Adresszusatz1 des Warenempfängers',
            'Adresszusatz2 des Warenempfängers',
            'MTS Artikelnummer',
            'Käufer Artikelnummer',
            'Artikelbezeichnung',
            'Artikelmenge',
            'Mengen-Einheit',
            'Eingabe-Nr.',
            'Endkundennummer',
        ];

        $columnDiff = array_diff($this->getColumnOrder(), array_keys($data), $columnsNotToImport);
        if (count($columnDiff) > 0) {
            throw new CsvImportException(sprintf('Missing column "%s" in input data.', array_pop($columnDiff)));
        }

        $this->setTrackingCode($data);
        $this->initDispatch($data);
        $this->markForExport();

        $this->entity->setInternalComment(
            (strlen($this->entity->getInternalComment()) > 0 ? $this->entity->getInternalComment() . "\n" : '') .
            $data['Notiz']
        );
        $this->entity->updateChangedTimestamp();
    }

    /**
     * Set the trackingcode for the order position.
     *
     * @param $data
     *
     * @throws CsvImportException
     */
    private function setTrackingCode($data): void
    {
        $positionStatusHandler = Shopware()->Container()->get('udg_logistic.position_status');
        $updateDetail = false;
        foreach ($this->entity->getDetails() as $detail) {
            /* @var $detail \Shopware\Models\Order\Detail */
            if ($detail->getEan() === $data['EAN']) {
                $detail->getAttribute()->setUdgDsBroadcastident($data['Sendungsverfolgungsnummer']);
                $detail->getAttribute()->setUdgDsPositionstatus($data['Shopware-Status']); // keep raw status for monitoring
                $positionStatusHandler->setPositionStatus($detail->getId(), $data);

                $updateDetail = true;
            }
        }

        if (!$updateDetail) {
            throw new CsvImportException(
                sprintf(
                    'Can\'t find order detail with ean "%s" for order "%s".',
                    $data['EAN'],
                    $data['Shopware-Ordernummer']
                )
            );
        }
    }

    /**
     * Set provided disptach for order, if dispatch is not present in `s_premium_dispatch` a new entry will be created
     *
     * @param array $data
     */
    private function initDispatch(array $data): void
    {
        if ($data['Logistikunternehmen'] === '') {
            return;
        }

        $dispatchRepository = $this->modelsService->getRepository(Dispatch::class);
        /** @var Dispatch $newDispatch */
        $dsDispatch = $dispatchRepository->findOneBy([
            'name'    => $data['Logistikunternehmen'],
            'comment' => self::DISPATCH_DS_COMMENT,
            'active'  => self::DISPATCH_DS_STATUS,
            'type'    => self::DISPATCH_DS_TYPE,
        ]);

        if ($dsDispatch === null) {
            // TODO: log dispatch creation
            $dsDispatch = new Dispatch();
            $dsDispatch->setName($data['Logistikunternehmen']);
            $dsDispatch->setComment(self::DISPATCH_DS_COMMENT);
            $dsDispatch->setActive(self::DISPATCH_DS_STATUS);
            $dsDispatch->setType(self::DISPATCH_DS_TYPE);

            // mandatory fields
            $dsDispatch->setDescription('');
            $dsDispatch->setPosition('');
            $dsDispatch->setCalculation('');
            $dsDispatch->setSurchargeCalculation('');
            $dsDispatch->setTaxCalculation('');
            $dsDispatch->setBindLastStock('');

            $this->modelsService->persist($dsDispatch);
        }

        $this->entity->setDispatch($dsDispatch);
    }

    /**
     * Mark order for next order status export.
     */
    private function markForExport(): void
    {
        $orderAttributeModel = $this->modelsService->getRepository(AttributeOrder::class)->findOneBy(
            ['orderId' => $this->entity->getId()]
        );

        if ($orderAttributeModel instanceof AttributeOrder) {
            $orderAttributeModel->setUdgLastexportedStatus(0);
            $this->modelsService->persist($orderAttributeModel);
        }
    }

}
