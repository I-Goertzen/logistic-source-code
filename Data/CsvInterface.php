<?php
declare(strict_types=1);

namespace UdgLogistic\Data;

/**
 * Interface for order export objects
 */
interface CsvInterface
{
    /**
     * Get the order of columns/properties.
     *
     * @return array
     */
    public function getColumnOrder(): array;

    /**
     * Convert object to an (multiple) array of the csv-rows.
     *
     * @return array
     */
    public function toArrayInColumnOrder(string $orderType = ''): array;

    /**
     * Update entity with Data
     *
     * @return void
     * @throws \Exception
     */
    public function updateEntityWithDataArray(array $data): void;
}
