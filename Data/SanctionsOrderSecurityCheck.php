<?php
declare(strict_types=1);

namespace UdgLogistic\Data;

use Shopware\Components\Model\ModelManager;
use Shopware\Models\Attribute\Article as AttributeArticle;
use Shopware\Models\Country\Country;
use Shopware\Models\Country\State;
use Shopware\Models\Customer\Address;
use Shopware\Models\Customer\Customer;
use Shopware\Models\Customer\Group as CustomerGroup;
use Shopware\Models\Dispatch\Dispatch;
use Shopware\Models\Order\Billing;
use Shopware\Models\Order\Detail;
use Shopware\Models\Order\DetailStatus;
use Shopware\Models\Order\Order as OrderEntity;
use Shopware\Models\Order\Shipping;
use Shopware\Models\Order\Status;
use Shopware\Models\Payment\Payment;
use Shopware\Models\Shop\Shop;
use Shopware\Models\Partner\Partner;
use UdgLogistic\Exception\CsvImportException;
use UdgLogistic\Exception\DataGenerationException;
use UdgLogistic\Service\OrderItemTypeIdentifier;
use UdgMerchants\Service\StockSplitting;

/**
 * Data mapping object for orm-entity to csv.
 */
class SanctionsOrderSecurityCheck implements CsvInterface
{

    /**
     * @var OrderEntity
     */
    private $entity;

    /**
     * @var Address
     */
    private $address;

    /**
     * @var \DateTime
     */
    private $dateExported;

    /**
     * @var \Shopware_Components_Config
     */
    private $configService;

    /**
     * @var ModelManager
     */
    private $modelsService;

    /**
     * @var Partner
     */
    private $partner;

    /**
     * @var $orderItemType
     */
    private $orderItemType;

    /**
     * Creates the Object from an orm entity.
     *
     * @param OrderEntity $entity
     * @param \DateTime   $dateExported
     *
     * @return Order
     */
    public static function createFromEntity(
        OrderEntity $entity,
        \DateTime $dateExported = null
    ): self {

        $order = new self();

        $order->entity = $entity;
        $order->dateExported = $dateExported;
        return $order;
    }

    /**
     * Get the current entity.
     *
     * @return OrderEntity
     */
    public function getEntity(): OrderEntity
    {
        return $this->entity;
    }

    /**
     * Inject Config.
     *
     * @param \Shopware_Components_Config $config
     */
    public function setConfigService(\Shopware_Components_Config $config): void
    {
        $this->configService = $config;
    }

    /**
     * Inject model manager.
     *
     * @param ModelManager $models
     */
    public function setModelsService(ModelManager $models): void
    {
        $this->modelsService = $models;
    }

    /**
     * Get array with column ordering
     *
     * @return array
     */
    public function getColumnOrder(): array
    {
        return [
            'FR-TTMmts                     TracTM'
        ];
    }

    /**
     * Get all csv rows for this order (multiple for details/variants)
     *
     * @return array
     * @throws DataGenerationException
     */
    public function toArrayInColumnOrder(string $orderType = ''): array
    {
        $data = [];

        $baseValues = $this->getBaseValues();

        /** @var Detail $article */
            foreach ($this->getColumnOrder() as $columnName) {
                if (array_key_exists($columnName, $baseValues)) {
                    $outValue[] = $baseValues[$columnName];
                } else {
                    throw new DataGenerationException(sprintf('Missing columnvalue for column: %s', $columnName));
                }
            }

            $data[] = $outValue;
        //}

        /* SORT DATA BY SHIPPING-CODE (ArrayKey 34 = ShippingCode) */
        usort($data, function ($x, $y, $key = 20) {
            return $x[$key] < $y[$key] ? -1 : $x[$key] != $y[$key];
        });
        return $data;
    }

    /**
     * Get base information.
     *
     * @return array
     */
    private function getBaseValues(): array
    {
        /** @var \Shopware\Models\Attribute\Order $attribute */
        $attributes = $this->entity->getAttribute();

        /** @var Shipping $shipping */
        $shipping = $this->entity->getShipping();

        /** @var Billing $billing */
        $billing = $this->entity->getBilling();

        $phone = (string)$shipping->getPhone();
        if ($phone !== '' && $billing instanceof Billing) {
            $phone = $billing->getPhone();
        }

        $gln = $attributes->getUdgDsMerchant();
        if (empty($gln) &&
            $this->entity->getTransactionId() !== '' &&
            !in_array(
                $this->entity->getDeviceType(),
                ['desktop', 'tablet', 'tabletLandscape', 'mobile', 'mobileLandscape']
            )
        ) {
            $gln = $this->entity->getDeviceType();
        }
        if (empty($gln)) {
            $gln = StockSplitting::WEBSHOP_GLN;
        }

        /** @var State $shippingState */
        $shippingState = $shipping->getState();
        $username = $this->getDataWithSpaces(trim($shipping->getFirstName().$shipping->getLastName()), 50);
        $street = $this->getDataWithSpaces(str_replace(' ','', $shipping->getStreet()), 35);
        $city = $this->getDataWithSpaces($shipping->getCity(), 24);
        $zip = $this->getDataWithSpaces($shipping->getZipCode(), 8);


        return [
            'FR-TTMmts                     TracTM'                     => 'TTSLmts                 mts     CN sw'.$this->entity->getNumber().'                         sw'.$this->entity->getNumber().'                   '.$this->entity->getNumber().'          '.$username.'                                                                      '.$street.'                                                                      '.$zip.$city.$shipping->getCountry()->getIso().'                                   '.$attributes->getUdgDsDeliverydate().' '
        ];
    }

    /**
     * Update order with data from csv.
     *
     * @param array $data
     *
     * @throws CsvImportException
     */
    public function updateEntityWithDataArray(array $data): void
    {
        $columnsNotToImport = ['Aufklebertyp', 'Packstückkennzeichnung', 'Sendungsident', 'Versandkosten (netto)'];

        $columnDiff = array_diff($this->getColumnOrder(), array_keys($data), $columnsNotToImport);

        if (count($columnDiff) > 0) {
            throw new CsvImportException(sprintf('Missing column "%s" in input data.', array_pop($columnDiff)));
        }


        if (!$this->entity->getCustomer() instanceof Customer) {
            $this->initCustomer($data);
            $this->initAddress($data);
            $this->initPartner('Drop Shipment');
            $this->initOrder($data);
        }

        $this->setArticleDetailValues($data);
        $this->entity->updateChangedTimestamp();
    }

    /**
     * @return Address
     */
    public function getAddress(): Address
    {

        return $this->address;
    }

    /**
     * Init an customer new entity.
     *
     * @param array $data
     *
     * @throws CsvImportException
     */
    private function initCustomer(array $data): void
    {
        $customer = new Customer();

        $customer->setAccountMode(1);

        $customer->setLastname(trim($data['Name']));
        $customer->setFirstname(' ');

        if ($data['E-Mail'] !== '') {
            $customer->setEmail($data['E-Mail']);
        } else {
            // TODO use setting
            $customer->setEmail('no-email@mts.de');
        }

        $customer->setSalutation('mr');

        $shopRepository = $this->modelsService->getRepository(Shop::class);
        $shopEntity = $shopRepository->getDefault();
        $customer->setShop($shopEntity);

        $customer->setGroup($this->getCustomerGroup());

        $this->entity->setCustomer(
            $customer
        );
    }

    /**
     * Init address from data.
     *
     * @param array $data
     *
     * @throws CsvImportException
     */
    private function initAddress(array $data): void
    {

        $this->address = new Address();
        $this->address->setTitle($data['Titel']);
        $this->address->setAdditionalAddressLine1($data['Adresszusatz1']);
        $this->address->setAdditionalAddressLine2($data['Adresszusatz2']);
        $this->address->setCompany($data['Firma']);
        $this->address->setDepartment($data['Abteilung']);

        $this->address->setSalutation($this->entity->getCustomer()->getSalutation());
        $this->address->setFirstname($this->entity->getCustomer()->getFirstname());
        $this->address->setLastname($this->entity->getCustomer()->getLastname());
        $this->address->setStreet($data['Straße, Hausnr']);
        $this->address->setZipcode($data['PLZ']);
        $this->address->setCity($data['Stadt']);
        $this->address->setPhone($data['Telefon']);

        $country = $this->modelsService->getRepository(Country::class)->findOneBy([
            'iso' => $data['Land'],
        ]);
        if (!$country instanceof Country) {
            throw new CsvImportException(sprintf('Can not find a country for `%s`', $data['Land']));
        }
        $this->address->setCountry($country);

        if ($data['Bundesland'] !== '') {
            $state = $this->modelsService->getRepository(State::class)->findOneBy([
                'shortCode' => $data['Bundesland'],
            ]);
            if (!$state instanceof State) {
                throw new CsvImportException(sprintf('Can not find a state for `%s`', $data['Bundesland']));
            }
            $this->address->setState($state);
        }
    }

    /**
     * init order
     *
     * @param array $data
     *
     * @throws \Exception
     */
    private function initOrder(array $data): void
    {
        // Setting default values, necessary because of not-nullable table colums
        $this->entity->setComment('');
        $this->entity->setCustomerComment('');
        $this->entity->setInternalComment('');
        $this->entity->setTemporaryId('');
        $this->entity->setTransactionId($data['Referenz']);
        $this->entity->setTrackingCode('');
        $this->entity->setReferer('');

        $this->entity->setCurrency('EUR');
        $this->entity->setCurrencyFactor(1);
        $this->entity->setLanguageIso('1');
        $this->entity->setTaxFree(0);
        $this->entity->setNet(0);
        $this->entity->setInvoiceAmount(0);
        $this->entity->setInvoiceAmountNet(0);
        $this->entity->setInvoiceShipping(0);
        $this->entity->setInvoiceShippingNet(0);
        $this->entity->setOrderStatus(
            $this->modelsService->getRepository(Status::class)->findOneBy([
                'id'    => Status::ORDER_STATE_OPEN,
                'group' => 'state',
            ])
        );
        $this->entity->setPaymentStatus(
            $this->modelsService->getRepository(Status::class)->findOneBy([
                'id'    => Status::PAYMENT_STATE_OPEN,
                'group' => 'payment',
            ])
        );

        $this->entity->setShop(
            $this->entity->getCustomer()->getShop()
        );
        $this->entity->setDispatch(
            $this->modelsService->getRepository(Dispatch::class)->findOneBy([
                // Standardversand
                'id'     => 9,
                'active' => 1,
            ])
        );
        $this->entity->setPayment(
            $this->modelsService->getRepository(Payment::class)->findOneBy([
                'name' => 'invoice',
            ])
        );
        $this->entity->setOrderTime(new \DateTime);

        $this->entity->setDeviceType('Drop Shipment');
        $this->entity->setPartner($this->partner);

        // add order attributes
        $orderAttribute = new \Shopware\Models\Attribute\Order();
        foreach ([
                     'GLN'                           => 'udgDsMerchant',
                     'Portal-Nummer'                 => 'udgDsPortalnumber',
                     'ERP-Nummer'                    => 'udgDsErpnumber',
                     'Wunschlieferdatum'             => 'udgDsWishdeliverydate',
                     'Lieferscheinnummer'            => 'udgDsReceiptnumber',
                     'voraussichtliches Lieferdatum' => 'udgDsDeliverydate',
                     'Alternative Benutzer ID'       => 'udgDsAdditionalcustomernumber',
                     'Endkundennummer'               => 'udgDsCustomernumberforendcustomer',
                 ] as $arrayKey => $propertyName) {
            $propertySetter = 'set' . ucfirst($propertyName);
            $orderAttribute->$propertySetter($data[$arrayKey]);
        }
        $this->entity->setAttribute($orderAttribute);
    }

    /**
     * Set the order article details.
     *
     * @param array $data
     *
     * @throws CsvImportException
     */
    private function setArticleDetailValues(array $data): void
    {

        $orderArticleDetail = new Detail();
        $orderArticleDetail->setOrder($this->entity);

        /** @var \Shopware\Models\Article\Detail $articleDetail */
        $articleDetail = $this->modelsService->getRepository(\Shopware\Models\Article\Detail::class)->findOneBy([
            'ean' => $data['EAN'],
        ]);

        if (!$articleDetail instanceof \Shopware\Models\Article\Detail) {
            throw new CsvImportException(
                sprintf('Missing article for EAN %s', $data['EAN'])
            );
        }

        $orderArticleDetail->setArticleNumber($articleDetail->getNumber());
        $orderArticleDetail->setEan($articleDetail->getEan());
        $orderArticleDetail->setArticleId($articleDetail->getArticleId());
        $orderArticleDetail->setArticleDetail($articleDetail);
        $orderArticleDetail->setArticleName($articleDetail->getArticle()->getName());
        $orderArticleDetail->setTax($articleDetail->getArticle()->getTax());
        $orderArticleDetail->setTaxRate($articleDetail->getArticle()->getTax()->getTax());
        // TODO ?
        $orderArticleDetail->setPrice($data['Preis']);
        $orderArticleDetail->setQuantity((int)$data['Menge']);
        $orderArticleDetail->setConfig($data['Besonderheiten']);

        $orderArticleDetail->setStatus(
            $this->modelsService->getRepository(DetailStatus::class)->findOneBy(['id' => 0])
        );

        $attribute = new \Shopware\Models\Attribute\OrderDetail();

        foreach ([
                     'Positionsnummer'      => 'udgDsPositionumber',
                     'Käufer Artikelnummer' => 'udgDsBuyerarticlenumber',
                     'Eingabe-Nr.'          => 'udgDsDocumentnumber',
                     'Location'             => 'udgDsLocation',
                     'Retourenschlüssel'    => 'udgDsReturnkey',
                 ] as $columnKey => $attributeKey) {
            $setter = 'set' . ucfirst($attributeKey);
            $attribute->$setter($data[$columnKey]);
        }

        $orderArticleDetail->setAttribute($attribute);

        $this->entity->getDetails()->add($orderArticleDetail);
    }


    /**
     * @param string $idCode
     *
     * @throws \Exception
     */
    private function initPartner(string $idCode): void
    {
        // try to find partner
        $partner = $this->modelsService->getRepository(Partner::class)->findOneBy([
            'idCode' => $idCode,
        ]);

        if ($partner !== null) {
            $this->partner = $partner;
        } else {
            // make new partner if necessary
            $this->partner = new Partner();
            $this->partner->setIdCode($idCode);
            $this->partner->setDate(new \DateTime('Now'));
            // this is done for nicer display in Shopware BE
            $this->partner->setCompany($idCode);

            // set empty values
            $this->partner->setContact('');
            $this->partner->setStreet('');
            $this->partner->setCity('');
            $this->partner->setZipCode('');
            $this->partner->setCountryName('');
            $this->partner->setPhone('');
            $this->partner->setFax('');
            $this->partner->setEmail('');
            $this->partner->setWeb('');
            $this->partner->setProfile('');
            $this->partner->setActive(1);

            $this->modelsService->persist($this->partner);
        }
    }

    /**
     * Get the customer group.
     *
     * @return CustomerGroup
     * @throws CsvImportException
     */
    private function getCustomerGroup(): CustomerGroup
    {
        // try to find customer group
        $customerGroup = $this->modelsService->getRepository(CustomerGroup::class)->findOneBy([
            'key'  => 'DS',
            'name' => 'DS-Kunde',
        ]);

        if ($customerGroup instanceof CustomerGroup) {
            return $customerGroup;
        }

        throw new CsvImportException('Can not find customer group "DS"');
    }

    private function getDataWithSpaces($str, $length){
        $strLength = strlen($str);
        $i = $length - $strLength;
        for($i;$i > 0; $i--){
            $str .= ' ';
        }
        return $str;
    }
}
