<?php
declare(strict_types=1);

namespace UdgLogistic\Data;

use Shopware\Models\Article\Detail;
use Shopware\Models\Attribute\Article as AttributeArticle;
use UdgLogistic\Exception\DataGenerationException;

/**
 * Data mapping object for orm-entity to csv.
 */
class Stock implements CsvInterface
{

    /**
     * @var Detail
     */
    private $entity;

    /**
     * @var int
     */
    private $currentInStock;

    /**
     * @var \DateTime
     */
    private $exportDate;

    /**
     * @var int
     */
    private $exportNumber;

    /**
     * @var string
     */
    private $gln;

    /**
     * @var array
     */
    protected $exportedStockIds = [];

    /**
     * Creates the Object from an orm entity.
     *
     * @param Detail $entity
     *
     * @return Stock
     */
    public static function createFromEntity(
        Detail $entity
    ): self {
        $stock = new self();
        $stock->entity = $entity;

        return $stock;
    }

    /**
     * Get the current entity.
     *
     * @return Detail
     */
    public function getEntity(): Detail
    {
        return $this->entity;
    }

    /**
     * @param int       $currentInStock
     * @param \DateTime $exportDate
     * @param int       $exportNumber
     */
    public function setAdditionalExportValues(int $currentInStock, string $gln, \DateTime $exportDate, int $exportNumber): void
    {
        $this->currentInStock = $currentInStock;
        $this->gln = $gln;
        $this->exportDate = $exportDate;
        $this->exportNumber = $exportNumber;
    }


    /**
     * Get article detail information.
     * @return array
     */
    private function getBaseValues(): array
    {

        return [
            'Bestandsberichtnummer' => $this->exportNumber,
            'Erstellungsdatum'      => $this->exportDate->format('Ymd'),
            'GLN'                   => $this->gln,
            'EAN'                   => $this->entity->getEan(),
            'Bestand aktuell'       => $this->currentInStock,
            'MTS_Artikelnummer'     => $this->getUdgDsMtsArticlenumber(),
        ];
    }


    /**
	 * Get the mts article number
     * @return string
     */
    private function getUdgDsMtsArticlenumber(): string
    {

        $attribute = $this->entity->getAttribute();

        if ($attribute === null || !$attribute instanceof AttributeArticle) {
            return '';
        }

        return (string)$attribute->getUdgDsMtsarticlenumber();
    }

    /**
     * Update order with data from csv.
     *
     * @param array $data
     *
     */
    public function updateEntityWithDataArray(array $data): void
    {
        $this->entity->setInStock($data['Gesamtbestand']);
    }

    /**
     * Get the order of columns/properties.
     * @return array
     */
    public function getColumnOrder(): array
    {
        return [
            'Bestandsberichtnummer',
            'Erstellungsdatum',
            'GLN',
            'EAN',
            'Bestand aktuell',
            'MTS_Artikelnummer',
        ];
    }

    /**
     * Convert object to an (multiple) array of the csv-rows.
     * @return array
     * @throws DataGenerationException
     */
    public function toArrayInColumnOrder(string $orderType = ''): array
    {
        $data = [];
        $baseValues = $this->getBaseValues();

        foreach ($this->getColumnOrder() as $columnName) {
            if (array_key_exists($columnName, $baseValues)) {
                $data[] = $baseValues[$columnName];
            } else {
                throw new DataGenerationException(sprintf('Missing columnvalue for column: %s', $columnName));
            }
        }

        return $data;
    }
}
