<?php
declare(strict_types=1);

namespace UdgLogistic\ExportAdapters;

use Shopware\Models\Order\Order;
use Shopware\Models\Attribute\Order as AttributeOrder;

/**
 * Export process for SanctionsTest order csv files.
 */
final class SanctionsTest extends SanctionsOrders
{

    /**
     * SanctionsTest constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {

        $this->config = [
            'filename' => 'ttls-mts-%s.csv',
            'filenameDatetimeFormat' => 'Y-m-d-H-i-sv',
            'ftpExportPath' => '/export/TTLS/',
        ];

        parent::__construct($config);
    }

    /**
     * Get the type-name of this export process.
     *
     * @return string
     */
    public function getType(): string
    {
        return 'sanctions-test-csv';
    }

    /**
     * update export date for all exported orders.
     *
     * @param array $exportedOrderIds
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function updateExportDate(array $exportedOrderIds): void
    {

        foreach ($exportedOrderIds as $orderId) {
            $orderAttributeModel = $this->modelsService->getRepository(AttributeOrder::class)->findOneBy(
                ['orderId' => $orderId]
            );

            if ($orderAttributeModel instanceof AttributeOrder) {
                $orderAttributeModel->setUdgLastexportedSap($this->dateExported->getTimestamp());

                $this->modelsService->persist($orderAttributeModel);
            }
        }

        $this->modelsService->flush();
    }

    /**
     * get order entities from database.
     * @return array
     */
    protected function getEntityObjects(): array
    {
        /**  @var \Shopware\Models\Order\Repository $repository */
        $repository = $this->modelsService->getRepository(Order::class);

        $date = new \DateTime();
        $date->modify('-1 hour');
        $orders = $repository->getOrdersQuery([
            ['property' => 'orders.orderTime', 'expression' => '>=', 'value' => $date]
        ]);

        return $orders->execute();
    }
}
