<?php
declare(strict_types=1);

namespace UdgLogistic\ExportAdapters;

use Shopware\Models\Order\Order;
use Shopware\Models\Attribute\Order as AttributeOrder;
use Shopware\Models\Order\Status;
use UdgLogistic\Data\OrderStatus;
use UdgLogistic\Exception\MissingConfigOptionException;
use UdgLogistic\Traits\ConfigSetting;
use UdgLogistic\Traits\InjectServices;
use UdgLogistic\UdgLogistic;
use UdgMerchants\Models\Merchant;
use UdgMerchants\Service\StockSplitting;

/**
 * Export Adapter for Order Status
 */
class OrdersStatus implements ExportAdaptersInterface
{
    use InjectServices;
    use ConfigSetting;

    /**
     * @var array
     */
    protected $exportedOrderIds = [];

    /**
     * @var array
     */
    protected $csvData = [];

    /**
     * @var \DateTime
     */
    protected $dateExported;

    /**
     * Orders constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = [
            'filename'               => 'export_order_status_%s.csv',
            'filenameDatetimeFormat' => 'Ymd_His_v',
            'ftpExportPath'          => '/export/order_status/',
        ];

        $this->setConfig($config);
    }

    /**
     * @throws \Exception
     */
    public function __clone()
    {
        $this->exportedOrderIds = [];
        $this->dateExported = new \DateTime();
    }

    /**
     * Creates new export process.
     *
     * @return ExportAdaptersInterface
     */
    public function create(): ExportAdaptersInterface
    {
        return clone $this;
    }

    /**
     * Get the type-name of this export process.
     *
     * @return string
     */
    public function getType(): string
    {
        return 'order-status-csv';
    }

    /**
     * Processing the export.
     * @throws \League\Csv\CannotInsertRecord
     * @throws \League\Csv\Exception
     * @throws \UdgLogistic\Exception\DataGenerationException
     * @throws \UdgRemoteFile\Exception\FileExistsException
     * @throws \UdgRemoteFile\Exception\FileNotFoundException
     * @throws \UdgRemoteFile\Exception\IOException
     * @throws \UdgLogistic\Exception\MissingConfigOptionException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function process(): void
    {

        foreach ($this->getMerchants() as $merchant) {
            /** @var Merchant $merchant */

            if ($merchant->getName() === StockSplitting::WEBSHOP_GLN) {
                continue;
            }

            $merchantGln = $merchant->getGln();

            foreach ($this->getEntityObjects() as $entity) {
                /* @var Order $entity */
                if ($entity->getAttribute()->getUdgDsMerchant() !== $merchantGln) {
                    continue;
                }
                /* Get current status and check, accept only 1,40 and 7 */
                $currentState = $entity->getOrderStatus()->getId();

                if ($currentState !== Status::ORDER_STATE_COMPLETELY_DELIVERED &&
                    $currentState !== Status::ORDER_STATE_IN_PROCESS &&
                    $currentState !== UdgLogistic::ORDER_STATE_PICKING
                ) {
                    continue;
                }

                $filename = $this->getFtpPathAndFilename($merchant, $currentState);
                $csvOrderStatus = OrderStatus::createFromEntity($entity);

                if (!array_key_exists($filename, $this->csvData)) {
                    $this->csvData[$filename] = [$csvOrderStatus->getColumnOrder()];
                }

                foreach ($csvOrderStatus->toArrayInColumnOrder() as $column) {
                    array_push($this->csvData[$filename], $column);
                }

                $this->exportedOrderIds[$filename][] = $entity->getId();
            }
        }
        if($this->csvData) {
            $writerContent = $this->makeWriterContent($this->csvData);

            foreach ($writerContent as $filename => $content){
                if (array_key_exists($filename, $this->exportedOrderIds) && count($this->exportedOrderIds[$filename])) {
                    // write content incl. create export directory
                    $this->ftpService->createDir($this->getConfigValue('ftpExportPath'));
                    $this->ftpService->write($filename . '.part', $content);

                    // update name on ftp server and update exportstatus in order
                    $this->ftpService->rename($filename . '.part', $filename);

                    $this->updateExportDate($this->exportedOrderIds[$filename]);
                }
            }
        }
    }

    /**
     * @param array $filenames
     * @param array $csvData
     * @return array
     */
    private function makeWriterContent(array $csvData): array
    {
        foreach($csvData as $key => $element) {
            $writer = $this->csvService->getWriter();
            $writer->insertAll($csvData[$key]);
            $output[$key] = $writer->getContent();
        }
        return $output;
    }


    /**
     * Get all merchants.
     * @return array
     */
    private function getMerchants(): array
    {
        $repository = $this->modelsService->getRepository(Merchant::class);

        return $repository->findAll();
    }

    /**
     * Get all orders that have not yet been exported.
     * @return array
     */
    protected function getEntityObjects(): array
    {
        /**  @var \Shopware\Models\Order\Repository $repository */
        $repository = $this->modelsService->getRepository(Order::class);

        $orders = $repository->getOrdersQuery([
            ['property' => 'attribute.udgLastexportedStatus', 'expression' => '=', 'value' => 0],
        ]);

        return $orders->execute();
    }

    /**
     * Update order status export timestamp to exported orders
     *
     * @param array $exportedOrderIds
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function updateExportDate(array $exportedOrderIds): void
    {
        foreach ($exportedOrderIds as $orderId) {
            $orderAttributeModel = $this->modelsService->getRepository(AttributeOrder::class)->findOneBy(
                ['orderId' => $orderId]
            );

            if ($orderAttributeModel instanceof AttributeOrder) {
                $orderAttributeModel->setUdgLastexportedStatus($this->dateExported->getTimestamp());

                $this->modelsService->persist($orderAttributeModel);
            }
        }

        $this->modelsService->flush();
    }


    /**
     * Get the full path and name forthe file.
     *
     * @param Merchant $merchant
     * @param String $wildcard
     *
     * @return string
     * @throws MissingConfigOptionException
     */
    protected function getFtpPathAndFilename(Merchant $merchant, int $currentState): string
    {
        $filename =
            sprintf(
                $this->getConfigValue('filename'),
                implode('_', [
                    $currentState,
                    preg_replace("/[^a-zA-Z0-9]+/", '', $merchant->getName()),
                    $this->dateExported->format($this->getConfigValue('filenameDatetimeFormat'))
                ])
            );
        return $this->getConfigValue('ftpExportPath') . $filename;
    }

    /**
     * final message of process run.
     * @return string
     * @throws MissingConfigOptionException
     */
    public function getFinalMessage(): string
    {
        $msg = '';
        foreach ($this->exportedOrderIds as $filename => $exportedOrderIds) {
            if (count($exportedOrderIds)) {
                $msg .= sprintf(
                    'file %1$s with %2$d orders was created',
                    $filename,
                    count($exportedOrderIds)
                );
            }
        }

        if ($msg !== '') {
            return $msg;
        }

        return 'Nothing to export';
    }
}