<?php
declare(strict_types=1);

namespace UdgLogistic\ExportAdapters;

/**
 * interface for all export adapters.
 */
interface ExportAdaptersInterface
{
    /**
     * Get the type-name of this export process.
     *
     * @return string
     */
    public function getType(): string;

    /**
     * Creates new export process.
     *
     * @return ExportAdaptersInterface
     */
    public function create(): ExportAdaptersInterface;

    /**
     * Processing the export.
     */
    public function process(): void;

    /**
     * final message of process run.
     * @return string
     */
    public function getFinalMessage(): string;
}
