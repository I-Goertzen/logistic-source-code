<?php
declare(strict_types=1);

namespace UdgLogistic\ExportAdapters;

use Shopware\Models\Order\Order;
use Shopware\Models\Order\Detail;
use UdgLogistic\Data\SanctionsOrderSecurityCheck as OrderData;
use UdgLogistic\Exception\MissingConfigOptionException;
use UdgLogistic\Traits\ConfigSetting;
use UdgLogistic\Traits\InjectServices;

/**
 * Export process for logistics order csv files.
 */
abstract class SanctionsOrders implements ExportAdaptersInterface
{
    use InjectServices;
    use ConfigSetting;

    /**
     * @var array
     */
    protected $exportedOrderIds = [];

    /**
     * @var \DateTime
     */
    protected $dateExported;

    /**
     * Orders constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->setConfig($config);
    }

    /**
     * @throws \Exception
     */
    public function __clone()
    {
        $this->exportedOrderIds = [];
        $this->dateExported = new \DateTime();
    }

    /**
     * Creates new export process.
     *
     * @return ExportAdaptersInterface
     */
    public function create(): ExportAdaptersInterface
    {
        return clone $this;
    }

    /**
     * Process export and upload of orders.
     *
     * @throws MissingConfigOptionException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \League\Csv\CannotInsertRecord
     * @throws \League\Csv\Exception
     * @throws \UdgLogistic\Exception\DataGenerationException
     * @throws \UdgRemoteFile\Exception\FileExistsException
     * @throws \UdgRemoteFile\Exception\FileNotFoundException
     * @throws \UdgRemoteFile\Exception\IOException
     */
    public function process($paramIsFromTestProcess = 0): void
    {
        $writer = $this->csvService->getWriter('');
        $finalArr = array();

        foreach ($this->getEntityObjects() as $entity) {
            //shipping address
            $entity = $this->initPositionNumberForEmptyPositionNumbers($entity);

            /* @var Order $entity */
            $csvOrder = OrderData::createFromEntity($entity, $this->dateExported);
            $csvOrder->setConfigService($this->configService);
            $csvOrder->setModelsService($this->modelsService);

            if (count($this->exportedOrderIds) === 0 && $paramIsFromTestProcess == 0) {
                // add csv heads
                $writer->insertOne($csvOrder->getColumnOrder());
            }
            $temp = $csvOrder->toArrayInColumnOrder($this->getType());

            $temp = $temp[0];

            if($paramIsFromTestProcess == 1) {
                $resultValue = $temp[0];

                $resultValue = str_replace(
                    " ",
                    "",
                    $resultValue
                );

                $finalArr[0] = $resultValue;
                $writer->insertAll(array($finalArr));
            }
            else{
                $resultValue = $temp[0];
                $finalArr[0] = $resultValue;
                $writer->insertAll(array($finalArr));
            }

            $this->exportedOrderIds[] = $entity->getId();
        }

        if($paramIsFromTestProcess == 1){}
        else {}

        if (count($this->exportedOrderIds)) {
            // write content incl. create export directory
            $this->ftpService->createDir($this->getConfigValue('ftpExportPath'));
            $this->ftpService->write($this->getFtpPathAndFilename() . '.part', $writer->getContent());

            // update name on ftp servern and update exportstatus in order
            $this->ftpService->rename($this->getFtpPathAndFilename() . '.part', $this->getFtpPathAndFilename());

            $this->updateExportDate($this->exportedOrderIds);
        }
    }

    /**
     * update export date for all exported orders.
     *
     * @param array $exportedOrderIds
     */
    abstract protected function updateExportDate(array $exportedOrderIds): void;

    /**
     * get order entities from database.
     * @return array
     */
    abstract protected function getEntityObjects(): array;


    /**
     * Init position number for an order.
     *
     * @param Order $order
     *
     * @return Order
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function initPositionNumberForEmptyPositionNumbers(Order $order): Order
    {
        $positionNumber = 1;
        $changed = false;

        foreach ($order->getDetails() as $orderDetail) {
            /* @var Detail $orderDetail */
            $orderDetailAttribute = $orderDetail->getAttribute();
            if (0 === (int)$orderDetailAttribute->getUdgDsPositionumber()) {
                $orderDetailAttribute->setUdgDsPositionumber($positionNumber);
                $this->modelsService->persist($orderDetailAttribute);
                $changed = true;
            } else {
                $positionNumber = max($positionNumber, $orderDetailAttribute->getUdgDsPositionumber());
            }
            $positionNumber++;
        }

        if (!$changed) {
            return $order;
        }

        $this->modelsService->flush();

        return $this->modelsService->getRepository(Order::class)->find($order->getId());
    }

    /**
     * Get the full path and name forthe file.
     *
     * @return string
     * @throws MissingConfigOptionException
     */
    protected function getFtpPathAndFilename(): string
    {
        $filename =
            sprintf(
                $this->getConfigValue('filename'),
                $this->dateExported->format($this->getConfigValue('filenameDatetimeFormat')).'-'.$this->getNumber());

        return $this->getConfigValue('ftpExportPath') . $filename;
    }

    public function getFilenameCounter(): string
    {
        $filename = 'New-file-count-number-'.$this->getNumber();

        return $filename;
    }

    /**
     * final message of process run.
     * @return string
     * @throws MissingConfigOptionException
     */
    public function getFinalMessage(): string
    {
        if (count($this->exportedOrderIds)) {
            $name = $this->getFtpPathAndFilename();
            $this->updateLastExportedNumber();
            return sprintf(
                'file %1$s with %2$d orders was created',
                $name,
                count($this->exportedOrderIds)
            );
        }
        return 'Nothing to export';
    }

    /**
     * @return \DateTime
     */
    public function getDateExported(): \DateTime
    {
        return $this->dateExported;
    }

    private function updateLastExportedNumber(){
        $sql = 'SELECT * FROM udg_general_settings where id=1';
        $dataT = Shopware()->Db()->fetchRow($sql);
        $sql = 'UPDATE udg_general_settings SET valu_text='.($dataT['valu_text']+1).'  WHERE id=1';
        Shopware()->Db()->executeQuery($sql);
    }

    private function getNumber(){
        $sql = 'SELECT * FROM udg_general_settings where id=1';
        $data = Shopware()->Db()->fetchRow($sql);
        $number = (string) ($data['valu_text']);
        $i = 6 - strlen($number);

        for($i; $i > 0; $i--){
            $number = '0'.$number;
        }
        return $number;
    }
}
