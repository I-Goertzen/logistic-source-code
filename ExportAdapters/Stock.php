<?php
declare(strict_types=1);

namespace UdgLogistic\ExportAdapters;

use Shopware\Models\Article\Detail;
use UdgHistory\Models\History;
use UdgHistory\Service\HistoryService;
use UdgLogistic\Exception\MissingConfigOptionException;
use UdgLogistic\Traits\ConfigSetting;
use UdgLogistic\Traits\InjectServices;
use UdgLogistic\Data\Stock as StockData;
use UdgMerchants\Models\Merchant;
use UdgMerchants\Service\StockSplitting;

/**
 * Export process for logistics order csv files.
 */
class Stock implements ExportAdaptersInterface
{
    use InjectServices;
    use ConfigSetting;

    /**
     * @var array
     */
    protected $exportedArticles = [];

    /**
     * @var \DateTime
     */
    protected $dateExported;

    /**
     * @var StockSplitting
     */
    protected $stockSplittingService;

    /**
     * Orders constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = [
            'filename'               => 'export_stock_%s.csv',
            'filenameDatetimeFormat' => 'Ymd_His_v',
            'ftpExportPath'          => '',
        ];

        $this->setConfig($config);
    }

    /**
     * @throws \Exception
     */
    public function __clone()
    {
        $this->exportedArticles = [];
        $this->dateExported = new \DateTime();
    }


    /**
     * Inject stock splitting service.
     *
     * @param StockSplitting $ftpService
     */
    public function setStockSplittingService(StockSplitting $stockSplittingService): void
    {
        $this->stockSplittingService = $stockSplittingService;
    }

    /**
     * Creates new export process.
     *
     * @return ExportAdaptersInterface
     */
    public function create(): ExportAdaptersInterface
    {
        return clone $this;
    }

    /**
     * Process export and upload of orders.
     *
     * @throws MissingConfigOptionException
     * @throws \League\Csv\CannotInsertRecord
     * @throws \League\Csv\Exception
     * @throws \UdgLogistic\Exception\DataGenerationException
     * @throws \UdgRemoteFile\Exception\FileExistsException
     * @throws \UdgRemoteFile\Exception\FileNotFoundException
     * @throws \UdgRemoteFile\Exception\IOException
     */
    public function process(): void
    {

        foreach ($this->getMerchants() as $merchant) {
            /** @var Merchant $merchant */

            $filename = $this->getFtpPathAndFilename($merchant);
            $writer = $this->csvService->getWriter();

            $exportNumber = 0;

            foreach ($this->getEntityObjects() as $entity) {
                /** @var Detail $entity */

                if (!$this->stockSplittingService->isMerchantArticle($entity, $merchant, true)) {
                    continue;
                }

                $csvStock = StockData::createFromEntity($entity);

                if (!array_key_exists($filename, $this->exportedArticles)) {
                    // add csv heads
                    $writer->insertOne($csvStock->getColumnOrder());
                    $this->exportedArticles[$filename] = 0;

                    $exportNumber = $this->getNextExportNumber();
                }

                $csvStock->setAdditionalExportValues(
                    $this->stockSplittingService->getStockByArticleDetailId($entity->getId(), $merchant->getGln()),
                    $merchant->getGln(),
                    $this->dateExported,
                    $exportNumber
                );

                $writer->insertOne($csvStock->toArrayInColumnOrder());
                $this->exportedArticles[$filename]++;
            }

            if (array_key_exists($filename, $this->exportedArticles)
                && $this->exportedArticles[$filename] > 0) {
                // write content incl. create export directory
                $this->ftpService->createDir($this->getConfigValue('ftpExportPath'));
                $this->ftpService->write($filename . '.part', $writer->getContent());

                // update name on ftp servern and update exportstatus in order
                $this->ftpService->rename($filename . '.part', $filename);
            }
        }
    }

    /**
     * Get the next export number.
     *
     * @return int
     */
    private function getNextExportNumber(): int
    {
        $exportNumber = 0;

        if (!$this->historyService instanceof HistoryService) {
            return 0;
        }

        $history = $this->historyService->getLastHistory('UdgLogistic', 'udg_logistic.export.stock-csv.exportNumber');

        if ($history instanceof History) {
            $exportNumber = (int)$history->getMessage();
        }
        $exportNumber++;

        $this->historyService->setHistory(
            'UdgLogistic',
            'udg_logistic.export.stock-csv.exportNumber',
            (string)$exportNumber
        );

        return $exportNumber;
    }

    /**
     * Get the type-name of this import process.
     *
     * @return string
     */
    public function getType(): string
    {
        return 'stock-csv';
    }

    /**
     * Get all merchants.
     * @return array
     */
    private function getMerchants(): array
    {
        $repository = $this->modelsService->getRepository(Merchant::class);

        return $repository->findAll();
    }

    /**
     * Get all articles.
     *
     * @return array
     */
    protected function getEntityObjects(): array
    {
        $repository = $this->modelsService->getRepository(Detail::class);

        return $repository->findAll();
    }

    /**
     * Get the filename.
     *
     * @param Merchant $merchant
     *
     * @return string
     * @throws MissingConfigOptionException
     */
    private function getFtpPathAndFilename(Merchant $merchant): string
    {
        $filename =
            sprintf(
                $this->getConfigValue('filename'),
                implode('_', [
                    preg_replace("/[^a-zA-Z0-9]+/", '', $merchant->getName()),
                    $this->dateExported->format($this->getConfigValue('filenameDatetimeFormat')),
                ])
            );

        return $this->getConfigValue('ftpExportPath') . $filename;
    }

    /**
     * final message of process run.
     * @return string
     * @throws MissingConfigOptionException
     */
    public function getFinalMessage(): string
    {
        $msg = '';
        foreach ($this->exportedArticles as $filename => $articleCount) {
            if (count($this->exportedArticles)) {
                $msg .= sprintf(
                    'file %1$s with %2$d articles was created. ',
                    $filename,
                    $articleCount
                );
            }
        }

        if ($msg !== '') {
            return $msg;
        }
        return 'Nothing to export';
    }
}
