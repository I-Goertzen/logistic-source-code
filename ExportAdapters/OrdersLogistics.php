<?php
declare(strict_types=1);

namespace UdgLogistic\ExportAdapters;

use Shopware\Models\Order\Order;
use Shopware\Models\Attribute\Order as AttributeOrder;
use Shopware\Models\Order\Status;

/**
 * Export process for logistics order csv files.
 */
final class OrdersLogistics extends Orders
{

    /**
     * OrdersLogistics constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {

        $this->config = [
            'filename' => 'export_order_logistic_%s.csv',
            'filenameDatetimeFormat' => 'Ymd_His_v',
            'ftpExportPath' => '/export/order_logistic/',
        ];

        parent::__construct($config);
    }

    /**
     * Get the type-name of this export process.
     *
     * @return string
     */
    public function getType(): string
    {
        return 'order-logistics-csv';
    }

    /**
     * update export date for all exported orders.
     *
     * @param array $exportedOrderIds
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function updateExportDate(array $exportedOrderIds): void
    {

        foreach ($exportedOrderIds as $orderId) {
            $orderAttributeModel = $this->modelsService->getRepository(AttributeOrder::class)->findOneBy(
                ['orderId' => $orderId]
            );

            if ($orderAttributeModel instanceof AttributeOrder) {
                $orderAttributeModel->setUdgLastexportedLogistic($this->dateExported->getTimestamp());

                $this->modelsService->persist($orderAttributeModel);
            }
        }

        $this->modelsService->flush();
    }

    /**
     * get order entities from database.
     * @return array
     */
    protected function getEntityObjects(): array
    {
        /**  @var \Shopware\Models\Order\Repository $repository */
        $repository = $this->modelsService->getRepository(Order::class);

        $orders = $repository->getOrdersQuery([
            ['property' => 'orders.status', 'expression' => '=', 'value' => Status::ORDER_STATE_OPEN],
            ['property' => 'attribute.udgLastexportedLogistic', 'expression' => '=', 'value' => 0],
        ]);

        return $orders->execute();
    }
}
