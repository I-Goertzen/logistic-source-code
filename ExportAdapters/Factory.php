<?php
declare(strict_types=1);

namespace UdgLogistic\ExportAdapters;

use IteratorAggregate;
use UdgLogistic\Exception\MissingAdapterException;

/**
 * Factory for generating export adapter.
 */
class Factory
{
    /**
     * @var ExportAdaptersInterface[]
     */
    private $adapters;

    /**
     * @param IteratorAggregate $adapterFactories
     */
    public function __construct(
        IteratorAggregate $adapterFactories
    ) {
        $this->adapters = iterator_to_array($adapterFactories, false);
    }

    /**
     * Return a new MediaService instance based on the configured storage type
     *
     * @param string $type
     *
     * @throws MissingAdapterException
     *
     * @return ExportAdaptersInterface
     */
    public function factory(string $type): ExportAdaptersInterface
    {
        foreach ($this->adapters as $adapter) {
            /* @var ExportAdaptersInterface $adapter */
            if ($adapter->getType() === $type) {
                return $adapter->create();
            }
        }

        throw new MissingAdapterException(sprintf('Adapter "%s" not found.', $type));
    }
}
